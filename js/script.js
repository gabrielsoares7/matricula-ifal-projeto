function matricular() {
    nome = document.getElementById("nome");
    cpf = document.getElementById("cpf");
    endereco = document.getElementById("endereco");
    email = document.getElementById("email");
    telefone = document.getElementById("telefone");

    formularioCompleto = true;
    correcao = "";

    //Validando nome
    if((nome.value == '') || ((nome.value.split(" ")).length < 2)) {
        correcao += "- Digite o seu nome COMPLETO;\n";
        formularioCompleto = false;
    }

    //Validando cpf
    if(cpf.value == '' || !isCpfValido(cpf.value)) {
        correcao += "- Digite um CPF válido;\n";
        formularioCompleto = false;
    }

    //Validando Email
    nomeDeUsuario = email.value.substring(0, email.value.indexOf("@"));
    dominio = email.value.substring(email.value.indexOf("@")+ 1, email.value.length);

    if (!((nomeDeUsuario.length >=1) &&
        (dominio.length >=3) &&
        (nomeDeUsuario.search("@")==-1) &&
        (dominio.search("@")==-1) &&
        (nomeDeUsuario.search(" ")==-1) &&
        (dominio.search(" ")==-1) &&
        (dominio.search(".")!=-1) &&
        (dominio.indexOf(".") >=1)&&
        (dominio.lastIndexOf(".") < dominio.length - 1))) {
        correcao += "- O email digitado é inválido;\n";
        formularioCompleto = false;
    }

    if(telefone.value == '' || telefone.value.length < 9) {
        correcao += "- O telefone digitado é inválido;\n"
    }


    if(formularioCompleto) {
        alert("A matrícula foi feita com sucesso!");
        window.location.replace("index.html");
    }
    else {
        alert(correcao);
    }
}

function isCpfValido(cpf) {
    if(cpf.length < 14)
        return false;
    else {
        soma = 0;
        cont = 1;
        for (i = 0; i < 11; i++) {
            if(i != 3 && i != 7) {
                soma += parseInt(cpf.substring(i, i + 1)) * (11 - cont);
                cont++;
            }
        }

        resto = (soma * 10) % 11;

        if ((resto == 10) || (resto == 11))
            resto = 0;

        if (resto != parseInt(cpf.substring(12, 13)) )
            return false;
        soma = 0;
        cont = 1;
        for (i = 0; i < 13; i++) {
            if(i != 3 && i != 7 && i != 11) {
                soma += parseInt(cpf.substring(i, i + 1)) * (12 - cont);
                cont++;
            }
        }

        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(13, 14) ) )
            return false;
        return true;
    }
}
